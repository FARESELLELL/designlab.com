---
name: Iconography
---

Icons are a valuable visual component to the GitLab brand; contributing to the overall visual language and user experience of a webpage, advertisement, or slide deck. The GitLab iconography currently consists of "label icons" and "content icons", each are explained in further detail below:

## Label icons

Label icons are intended to support usability and interaction. These are found in interactive elements of the website such as navigation and [toggles](https://about.gitlab.com/pricing/).

![Label icons](/img/brand/label-icons-example.png)

## Content icons

Content icons are intended to provide visual context and support to content on a webpage; these icons also have a direct correlation to our illustration style with the use of bold outlines and fill colors.

A few examples include our [event landing pages](https://about.gitlab.com/events/aws-reinvent/) and [Resources page](https://about.gitlab.com/resources/).

![Content icons](/img/brand/content-icons-example.png)
